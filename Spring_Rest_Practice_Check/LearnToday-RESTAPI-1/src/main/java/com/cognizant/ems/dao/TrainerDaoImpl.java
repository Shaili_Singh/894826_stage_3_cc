package com.cognizant.ems.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.cognizant.ems.entities.Trainer;





@Component
public class TrainerDaoImpl implements TrainerDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	@Override
	public boolean trainerSignUp(Trainer trainer) throws DuplicateKeyException {
		// TODO Auto-generated method stub
		

		int res=  jdbcTemplate.update("insert into trainer values(?,?)", trainer.getTrainerId(),trainer.getPassword());
		
		if(res>=1)
		{
			return true;
		}
		return false;
	}

	@Override
	public boolean updatePassword(int trainerId, Trainer trainer) {
		// TODO Auto-generated method stub
int res = jdbcTemplate.update("update trainer set password =? where trainerId=?", trainer.getPassword(),trainerId);
		
		if(res>=1)
		{
			return true;
		}
		return false;
	}

	@Override
	public Trainer find(int trainerid) {
		// TODO Auto-generated method stub
		return jdbcTemplate.query("select * from trainer where trainerId="+trainerid,new TrainerResultSet());

	}


}
