package com.cognizant.ems.controllers;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.ems.entities.CourseDTO;
import com.cognizant.ems.entities.Student;
import com.cognizant.ems.services.AdminService;
import com.cognizant.ems.services.StudentServices;

/****
 * 
 * @author Shivam
 *
 */
@RestController
@RequestMapping("/api")
public class StudentController {

	@Autowired
	private StudentServices service;
	
	@Autowired
	private AdminService aservice;

	
	@GetMapping("/Student")
	public ResponseEntity<CourseDTO>   getAllCourses()
	{
		CourseDTO dto = new CourseDTO();
		dto.setCourse(aservice.getAllCourses());
		
		
		
		  ResponseEntity<CourseDTO> responseEntity = new ResponseEntity<CourseDTO>(dto, HttpStatus.OK);
		  return responseEntity;
		
	}
	
	

	@PostMapping("/Student")
	@ResponseStatus(HttpStatus.CREATED)
	public  Student createNewDepartment(  @RequestBody @Valid Student st)
	{
		service.postStudent(st);
		
		return  st;
	}
	
	@DeleteMapping("/Student/{enrollmentId}")
	public ResponseEntity<?> deleteDepartment(@PathVariable("enrollmentId") int enrollmentId)
	{
		
		 service.deleteStudentEnrollment(enrollmentId);
		 
		 return new ResponseEntity<String>("Deleted Successfully",HttpStatus.OK);
		
		
		
	}
}
