package com.cognizant.ems.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.cognizant.ems.entities.Trainer;



public class TrainerResultSet  implements ResultSetExtractor<Trainer> {

	@Override
	public Trainer extractData(ResultSet rs) throws SQLException, DataAccessException {
		Trainer d= null;
		
		  
		     if(rs.next())
		     {
		    	 d = new Trainer();
		    		d.setTrainerId(rs.getInt(1));
		    		d.setPassword(rs.getString(2));
		    	
		     }
		
		return d;
	}

}
