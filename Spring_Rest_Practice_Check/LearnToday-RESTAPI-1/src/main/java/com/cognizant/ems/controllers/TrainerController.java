package com.cognizant.ems.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.ems.entities.Trainer;

import com.cognizant.ems.services.TrainerService;



@RestController
@RequestMapping("/api")
public class TrainerController {
	@Autowired
	TrainerService service;
	
	@PostMapping("/Trainer")
	@ResponseStatus(HttpStatus.CREATED)
	public  Trainer trainerSignUp(  @RequestBody @Valid Trainer trainer)
	{
		service.trainerSignUp(trainer);
		
		return  trainer;
	}
	
	@PutMapping("/Trainer/{trainerId}")
	public ResponseEntity<String> updateDepartment(@PathVariable("trainerId") int trainerId,@RequestBody Trainer trainer)
	{
		System.out.println(trainerId+"  "+trainer);
		service.updatePassword(trainerId,trainer);
		
		return new ResponseEntity<String>("Data Updated Successfully",HttpStatus.OK);
		
	}
	
	
	/*****
	 * 
	 * @author Shivam
	 */
}
