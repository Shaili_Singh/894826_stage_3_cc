package com.cognizant.ems.entities;

public class Student {

	int enrollmentId ;
	 int studentId ;
	 int courseId ;
	public int getEnrollmentId() {
		return enrollmentId;
	}
	public void setEnrollmentId(int enrollmentId) {
		this.enrollmentId = enrollmentId;
	}
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	@Override
	public String toString() {
		return "Student [enrollmentId=" + enrollmentId + ", studentId=" + studentId + ", courseId=" + courseId + "]";
	}
	 
	 
}
