package com.cognizant.ems.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import com.cognizant.ems.entities.Course;
import com.cognizant.ems.entities.Student;



public class CourseRowMapper implements RowMapper<Course> {
	@Autowired
	StudentDao dao;
	@Override
	public Course mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		
		Course d= new Course();
		d.setCourseId(rs.getInt(1));
		
		d.setTitle(rs.getString(2));
		d.setFees(rs.getFloat(3));
		d.setDescription(rs.getString(4));
		d.setTrainer(rs.getString(5));
		d.setStart_Date(rs.getDate(6));
		//List<Student> s=dao.courseWithId(rs.getInt(1));
		//d.setStudent(s);
		return d;
	}

}
