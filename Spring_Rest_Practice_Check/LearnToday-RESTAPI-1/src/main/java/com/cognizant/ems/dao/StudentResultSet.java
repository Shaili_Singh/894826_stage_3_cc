package com.cognizant.ems.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;


import com.cognizant.ems.entities.Student;

public class StudentResultSet implements ResultSetExtractor<Student> {

	@Override
	public Student extractData(ResultSet rs) throws SQLException, DataAccessException {
		  Student d= null;
		
		  
		     if(rs.next())
		     {
		    	 d = new Student();
		    		d.setEnrollmentId(rs.getInt(1));
		    		d.setStudentId(rs.getInt(2));
		    		d.setCourseId(rs.getInt(3));
		     }
		
		return d;
	}
 

	
}
