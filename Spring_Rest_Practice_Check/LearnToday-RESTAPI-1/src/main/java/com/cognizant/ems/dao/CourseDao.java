package com.cognizant.ems.dao;

import java.util.List;


/****
 * @author Shivam
 */
import com.cognizant.ems.entities.Course;

public interface CourseDao {

	public List<Course> getAllCourses();
	public Course getCourseById(int id);
}
