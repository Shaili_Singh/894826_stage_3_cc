package com.cognizant.ems.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.ems.dao.StudentDao;
import com.cognizant.ems.dao.TrainerDao;
import com.cognizant.ems.entities.Trainer;
import com.cognizant.ems.exception.BadResourceRequestException;
import com.cognizant.ems.exception.ResourceNotFoundException;


@Service
public class TrainerService {

	@Autowired
	private TrainerDao dao;
	

	public void trainerSignUp(Trainer trainer)
	{
		
		 Trainer existingtrainer = dao.find(trainer.getTrainerId());
		  if(existingtrainer!=null)
		  {
			  throw new BadResourceRequestException("Duplicate trainer with id "+trainer.getTrainerId());
		  }
		  else
		  {
			  dao.trainerSignUp(trainer);
		}
		
	}
	
	public void updatePassword(int trainerId,Trainer trainer) {
		Trainer Existingtrainer =	dao.find(trainerId);
		
		
		   if(Existingtrainer==null)
		   {
			   throw new ResourceNotFoundException("Searched Data not Found");
		   }else
		   {
			   dao.updatePassword(trainerId,trainer);
		   }
	}
}
