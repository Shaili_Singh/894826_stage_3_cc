package com.cognizant.ems.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.ems.dao.CourseDao;
import com.cognizant.ems.entities.Course;
import com.cognizant.ems.exception.ResourceNotFoundException;





@Service
public class AdminService {

	@Autowired
	CourseDao dao;
	
	public List<Course> getAllCourses()
	{
	return dao.getAllCourses();	
	}
	
	public Course getCourseById(int courseid) {
		Course dept =	dao.getCourseById(courseid);
		
		
		   if(dept==null)
		   {
			   throw new ResourceNotFoundException("Searched Data not found");
		   }
		return dept;
	}
	
}
