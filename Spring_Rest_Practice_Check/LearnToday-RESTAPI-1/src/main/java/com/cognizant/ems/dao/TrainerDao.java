package com.cognizant.ems.dao;

import org.springframework.dao.DuplicateKeyException;

import com.cognizant.ems.entities.Trainer;




public interface TrainerDao {
	boolean trainerSignUp(Trainer trainer) throws DuplicateKeyException;
	boolean updatePassword(int trainerId, Trainer trainer);
	public Trainer find(int trainerid);
}
