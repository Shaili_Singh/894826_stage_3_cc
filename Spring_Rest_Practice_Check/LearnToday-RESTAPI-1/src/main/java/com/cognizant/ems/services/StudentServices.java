package com.cognizant.ems.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.ems.dao.StudentDao;
import com.cognizant.ems.entities.Course;
import com.cognizant.ems.entities.Student;
import com.cognizant.ems.exception.BadResourceRequestException;
import com.cognizant.ems.exception.ResourceNotFoundException;



@Service
public class StudentServices {
	@Autowired
	private StudentDao dao;
	
	
	public void postStudent(Student st)
	{
		
		 Student existingenrollmentid = dao.find(st.getEnrollmentId());
		  if(existingenrollmentid!=null)
		  {
			  throw new BadResourceRequestException("Duplicate enrollment id "+st.getEnrollmentId());
		  }
	else
		  {
			  dao.postStudent(st);
		  }
		
	}
	
	
	
	
	public  void deleteStudentEnrollment(int enrollid)
	{
		Student st =	dao.find(enrollid);
		
		
		  if(st==null)
		   {
			   throw new ResourceNotFoundException("No enrollment information found");
		   }else
		   {
			   dao.deleteStudentEnrollment(enrollid);
		}
		   
		   
	}
	
	public List<Course> getAllCourses()
	{
		
		
	return dao.getAllCourses();	
	}
	
}
