package com.cognizant.ems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnTodayRestapi1Application {

	public static void main(String[] args) {
		SpringApplication.run(LearnTodayRestapi1Application.class, args);
	}

}
