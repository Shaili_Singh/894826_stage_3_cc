package com.cognizant.ems.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.ems.entities.Course;
import com.cognizant.ems.entities.CourseDTO;
import com.cognizant.ems.services.AdminService;



@RestController
@RequestMapping("/api")
class AdminController{
	
	@Autowired
	AdminService service;
	
	@GetMapping("/Admin")
	public ResponseEntity<CourseDTO>   getAllCourses()
	{
		CourseDTO dto = new CourseDTO();
		dto.setCourse(service.getAllCourses());
		
		
		
		  ResponseEntity<CourseDTO> responseEntity = new ResponseEntity<CourseDTO>(dto, HttpStatus.OK);
		  return responseEntity;
		
	}
	@GetMapping("/Admin/{courseid}")
	public ResponseEntity<Course> getDepartment(@PathVariable(name = "courseid") int courseid)
	{
		
		System.out.println(courseid);
		Course dept = service.getCourseById(courseid);
		System.out.println(dept);
		ResponseEntity<Course> deptResp = new ResponseEntity<Course>(dept,HttpStatus.OK);
	return deptResp;
	
	}
 }