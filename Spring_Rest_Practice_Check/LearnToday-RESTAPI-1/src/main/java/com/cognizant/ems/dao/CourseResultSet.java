package com.cognizant.ems.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.cognizant.ems.entities.Course;




public class CourseResultSet implements ResultSetExtractor<Course> {

	@Override
	public Course extractData(ResultSet rs) throws SQLException, DataAccessException {
		  Course d= null;
		
		  
		     if(rs.next())
		     {
		    	 d = new Course();
		    		d.setCourseId(rs.getInt(1));
		    		d.setTitle(rs.getString(2));
		    		d.setFees(rs.getFloat(3));
		    		d.setDescription(rs.getString(4));
		    		d.setTrainer(rs.getString(5));
		    		d.setStart_Date(rs.getDate(6));
		    		
		     }
		
		return d;
	}

}
