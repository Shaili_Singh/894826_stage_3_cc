package com.cognizant.ems.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import com.cognizant.ems.entities.Course;
import com.cognizant.ems.entities.Student;



@Component
public class StudentDaoImpl  implements StudentDao{

	@Autowired
	JdbcTemplate jdbcTemplate;

	
	@Override
	public List<Course> getAllCourses() {
		// TODO Auto-generated method stub
		
		
		return jdbcTemplate.query("select * from course order by Start_Date", new CourseRowMapper());

	}

	@Override
	public boolean postStudent(Student st) {
		// TODO Auto-generated method stub
int res=  jdbcTemplate.update("insert into student values(?,?,?)", st.getEnrollmentId(),st.getStudentId(),st.getCourseId());
		
		if(res>=1)
		{
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteStudentEnrollment(int enrollid) {
		// TODO Auto-generated method stub
int res=  jdbcTemplate.update("delete from student where EnrollmentId="+enrollid);
		
		if(res>=1)
		{
			return true;
		}
		return false;
	}

	@Override
	public Student find(int enrollid) {
		// TODO Auto-generated method stub
		return jdbcTemplate.query("select * from student where EnrollmentId="+enrollid,new StudentResultSet());

	}

	@Override
	public List<Student> courseWithId(int courseId) {
		// TODO Auto-generated method stub
		
		RowMapper<Student> studentRowMapper= (rs,rows)->
		{
			Student d =new  Student();
			d.setEnrollmentId(rs.getInt(1));
			d.setStudentId(rs.getInt(2));
			d.setCourseId(rs.getInt(3));
			return d;
			
		};
		
		List<Student> students= jdbcTemplate.query("select st.EnrollmentId, st.StudentId, st.CourseId from Student st, Course c where c.courseId = st.courseId and and c.courseId ="+courseId, studentRowMapper);
		return students;
		
		
		
	}

}
