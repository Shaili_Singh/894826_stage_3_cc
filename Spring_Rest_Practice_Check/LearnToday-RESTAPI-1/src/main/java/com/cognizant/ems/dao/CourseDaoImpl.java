package com.cognizant.ems.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.cognizant.ems.entities.Course;
import com.cognizant.ems.entities.Student;


@Component
public class CourseDaoImpl implements CourseDao{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Override
	public List<Course> getAllCourses() {
		// TODO Auto-generated method stub
		List<Course> courses= jdbcTemplate.query("select * from course", new CourseRowMapper());
		List<Course> coursesWithStudent = new ArrayList<Course>();
		
		Iterator<Course> itr = courses.iterator();
		while(itr.hasNext())
		{
			
			Course c = itr.next();
			List<Student> studentList = jdbcTemplate.query("select * from student where courseid="+c.getCourseId(), new StudentRowMapper());
			c.setStudent(studentList);
			coursesWithStudent.add(c);
		}
		
		
		
		
		
		return coursesWithStudent;
	}

	@Override
	public Course getCourseById(int id) {
		// TODO Auto-generated method stub
		return jdbcTemplate.query("select * from course where CourseId="+id,new CourseResultSet());

	}

}
