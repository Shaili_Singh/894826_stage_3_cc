package com.cognizant.ems.dao;

import java.util.List;

import com.cognizant.ems.entities.Course;
import com.cognizant.ems.entities.Student;



public interface StudentDao {

	public Student find(int enrollid);
	public List<Course> getAllCourses();
	public boolean postStudent(Student dept);
	public List<Student> courseWithId(int courseId);
	public boolean deleteStudentEnrollment(int enrollid);
}
