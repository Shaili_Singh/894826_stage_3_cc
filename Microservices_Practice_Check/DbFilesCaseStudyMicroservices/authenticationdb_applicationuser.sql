create database authenticationdb;
use authenticationdb;
DROP TABLE IF EXISTS `applicationuser`;

CREATE TABLE `applicationuser` (
  `userid` int NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

desc applicationuser;

INSERT INTO `applicationuser` VALUES (101,'shaili','1234'),(102,'ajay','1234'),(103,'shivam','1234');