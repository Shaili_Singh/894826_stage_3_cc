use orderdb;

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `orderid` int NOT NULL AUTO_INCREMENT,
  `userid` int DEFAULT NULL,
  `deliveryaddress` varchar(200) DEFAULT NULL,
  `orderdate` date DEFAULT NULL,
  `paymentmode` varchar(200) DEFAULT NULL,
  `orderamount` int DEFAULT NULL,
  PRIMARY KEY (`orderid`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `orders` VALUES (1,101,'kashipur','2021-04-10','online',4200),(2,101,'kashipur','2021-04-10','online',4200),(3,101,'kashipur','2021-04-10','online',4200),(4,101,'kashipur','2021-04-12','online',3200),(5,101,'kashipur','2021-04-12','online',3200),(6,101,'kashipur','2021-04-12','online',5800),(7,101,'kashipur','2021-04-12','online',5800),(8,101,'kashipur','2021-04-12','online',4400),(9,101,'kashipur','2021-04-12','online',5800),(10,101,'kashipur','2021-04-12','online',5800),(11,101,'kashipur','2021-04-12','online',6400),(12,101,'kashipur','2021-04-12','online',3200),(13,101,'kashipur','2021-04-12','online',3200),(14,101,'kashipur','2021-04-12','online',3200),(15,101,'kashipur','2021-04-12','online',3200);
