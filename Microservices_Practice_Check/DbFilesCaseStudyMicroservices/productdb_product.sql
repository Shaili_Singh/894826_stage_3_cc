create database productdb;
use productdb;
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `productId` int NOT NULL,
  `productName` varchar(30) DEFAULT NULL,
  `productPrice` int DEFAULT NULL,
  `productDescription` varchar(200) DEFAULT NULL,
  `productImageUrl` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `product` VALUES (201,'product1',600,'Product Description','www.productImage.com'),(202,'product2',2600,'Product Description','www.productImage.com');

select * from product;

