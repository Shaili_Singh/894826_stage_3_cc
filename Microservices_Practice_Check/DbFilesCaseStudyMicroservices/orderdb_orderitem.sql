create database orderdb;
use orderdb;

DROP TABLE IF EXISTS `orderitem`;

CREATE TABLE `orderitem` (
  `id` int NOT NULL AUTO_INCREMENT,
  `orderid` int DEFAULT NULL,
  `productid` int DEFAULT NULL,
  `productname` varchar(100) DEFAULT NULL,
  `productprice` int DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `subtotal` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `orderitem` VALUES (1,3,201,'product1',600,3,1800),(2,3,201,'product2',1200,2,2400),(3,4,201,'product1',600,1,600),(4,4,202,'product2',2600,1,2600),(5,5,201,'product1',600,1,600),(6,5,202,'product2',2600,1,2600),(7,6,201,'product1',600,1,600),(8,6,202,'product2',2600,1,2600),(9,6,202,'product2',2600,1,2600),(10,7,201,'product1',600,1,600),(11,7,202,'product2',2600,1,2600),(12,7,202,'product2',2600,1,2600),(13,8,201,'product1',600,1,600),(14,8,201,'product1',600,1,600),(15,8,202,'product2',2600,1,2600),(16,8,201,'product1',600,1,600),(17,9,201,'product1',600,1,600),(18,9,202,'product2',2600,1,2600),(19,9,202,'product2',2600,1,2600),(20,10,201,'product1',600,1,600),(21,10,202,'product2',2600,1,2600),(22,10,202,'product2',2600,1,2600),(23,11,202,'product2',2600,1,2600),(24,11,202,'product2',2600,1,2600),(25,11,201,'product1',600,1,600),(26,11,201,'product1',600,1,600),(27,12,201,'product1',600,1,600),(28,12,202,'product2',2600,1,2600),(29,13,201,'product1',600,1,600),(30,13,202,'product2',2600,1,2600),(31,14,201,'product1',600,1,600),(32,14,202,'product2',2600,1,2600),(33,15,201,'product1',600,1,600),(34,15,202,'product2',2600,1,2600);

select * from orderitem;