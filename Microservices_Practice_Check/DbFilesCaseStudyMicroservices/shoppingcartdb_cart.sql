create database shoppingcartdb;

use shoppingcartdb;

DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userid` int DEFAULT NULL,
  `productid` int DEFAULT NULL,
  `productname` varchar(100) DEFAULT NULL,
  `productprice` int DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `sumtotal` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=343 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

